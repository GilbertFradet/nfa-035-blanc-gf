# EXAMEN

Voici venu le jour de l'Examen Blanc

## QCM

**1. N-Tier est-il : (plusieurs réponses possibles)**


   - [ ] Un Design Pattern Objet ?
   - [x] Un Pattern d'Architecture Logicielle ?
   - [x] Un pattern d'architecture technique ? (dans le sens ou la persistance utilise une architecture technique avec l'emploi d'une machine dédiée à la persistance (BDD))


**2. Même question pour l'IOC, peut-il s'agir : (plusieurs réponses possibles)**


   - [x] D'un Design Pattern Objet ? (dans le sens ou l'on conçoit un ou des objets déstinés à être injectés)
   - [x] D'un Pattern d'Architecture Logicielle ? (dans le sens ou la conception du logiciel utilise des stucture fisant intervenir l'IOC)
   - [ ] D'un pattern d'architecture technique ?


**3. Comment qualifieriez-vous le découpage en couche ?**


   - [ ] En tant qu'architecture technique.
   - [x] En tant qu'architecture logicielle.
   - [x] En tant que design pattern. Exemple: DAO


**4. Qu'est-ce que POJO ? (plusieurs réponses possibles)**


  - [x] Un Plain Old Java Object
  - [x] Une librairie pour faire de l'IOC (dans le sens ou l'objet POJO est injecté)
  - [] Une librairie boîte à outil pour faire notamment de la sérialisation
  - [x] Un design pattern Objet (il s'agit d'un patron de design objet)

## EXERCICES

### Exercice 1: Patterns Singleton / Factory

- [ ] Implémenter **MyWhiteExam** comme POJO selon le diagramme suivant (ne pas oublier la méthode equals()):
```plantuml
@startuml
title __MODEL's Class Diagram__\n
        namespace fr.cnam.foad.nfa035.exam {
        namespace model {
class fr.cnam.foad.nfa035.exam.model.MyWhiteExam {
        - codeUe : String
        - date : Date
        - ecole : String
        - intitule : String
        - region : String
        + MyWhiteExam()
        + MyWhiteExam()
        + equals()
        + getCodeUe()
        + getDate()
        + getEcole()
        + getIntitule()
        + getRegion()
        + setCodeUe()
        + setDate()
        + setEcole()
        + setIntitule()
        + setRegion()
        + toString()
        }
        }
        }
@enduml 
```
- [ ] Implémenter la Classe **MyExamFactory** afin de permettre l'instanciation d'un objet **MyWhiteExam** Unique (Utilisation de l'annotation @Bean), par appel d'un constructeur, comme suit:
  ```java
  new MyWhiteExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date());
  ```
- [ ] S'assurer que les classes développées répondent bien aux exigences en exécutant le test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testSingletonFactory**

### Exercice 2: Pattern Delegate

- [ ] Sachant que la classe **MyExamFactory** vous permet d'injecter l'examen en cours dont le type est **MyWhiteExam**, développer la classe **MyExamDelegateContract**
- [ ] S'assurer que la classe développée répond bien aux exigences en exécutant le test **fr.cnam.foad.nfa035.exam.Nfa035ExamPatternTest#testDelegateToString**

____

## Annexes: diagrammes-classes



```plantuml
@startuml

  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      class fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate {
          + delegateToString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      interface fr.cnam.foad.nfa035.exam.pattern.MyExamDelegateContract {
          {abstract} + delegateToString()
      }
    }
  }


  namespace fr.cnam.foad.nfa035.exam {
    namespace pattern {
      class fr.cnam.foad.nfa035.exam.pattern.MyExamFactory {
          + getCurrentExam()
      }
    }
  }


  fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate .up.|> fr.cnam.foad.nfa035.exam.pattern.MyExamDelegateContract
  fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate o-left- fr.cnam.foad.nfa035.exam.model.MyWhiteExam : exam

namespace fr.cnam.foad.nfa035.exam {
    namespace model {
      class fr.cnam.foad.nfa035.exam.model.MyWhiteExam {
          - codeUe : String
          - date : Date
          - ecole : String
          - intitule : String
          - region : String
          + MyWhiteExam()
          + MyWhiteExam()
          + equals()
          + getCodeUe()
          + getDate()
          + getEcole()
          + getIntitule()
          + getRegion()
          + setCodeUe()
          + setDate()
          + setEcole()
          + setIntitule()
          + setRegion()
          + toString()
      }
    }
  }

@enduml

```



